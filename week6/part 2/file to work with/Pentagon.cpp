#include "Pentagon.h"
#include "MathUtils.h"

Pentagon::Pentagon(std::string name, std::string col, double side) : Shape(name, col)
{
	setSide(side);
}

void Pentagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl 
	<< "side is " << this->_side << std::endl << "Area is " << MathUtils::CalPentagonArea(this->_side) << std::endl ;
}

void Pentagon::setSide(int side)
{
	if (side < 0)
	{
		throw shapeException();
	}
	if (std::cin.fail())
	{
		throw InputException();
	}
	this->_side = side;
}



