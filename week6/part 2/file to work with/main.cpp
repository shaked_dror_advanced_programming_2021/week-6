#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "InputException.h"
#include "shapeException.h"
#include "Hexagon.h"
#include "Pentagon.h"


//will do the input from the user(numbers input)
int num_input()
{
	double x;
	std::cin >> x;
	if (std::cin.fail())
	{
		std::cin.clear();
		std::cin.ignore();
		throw InputException();
	}
	
	return x;
}

//will do char input, if input letters is more than 1 will print a msg
char char_input()
{
	//will get a string from a user
	std::string save = "";
	std::cin >> save;

	//if the onput is more than 1 will print an warning msg
	if (save.length() > 1)
	{
		std::cout << "Warning - Don't try to build more than one shape at once" << std::endl;
	}
	//return the first char in the string 
	return save[0];
}


int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0, side = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Hexagon hex(nam, col, side);
	Pentagon pen(nam, col, side);


	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape* ptrHex = &hex;
	Shape* ptrPen = &pen;


	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; char shapetype;
	char x = 'y';

	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, h = hexagon, e = pentagon" << std::endl;		
		shapetype = char_input();
		try
		{			
			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam;
				rad = num_input();

				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col;
				height = num_input();
				width = num_input();

				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col;
				height = num_input();
				width = num_input();

				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col;
				height = num_input();
				width = num_input();
				ang = num_input();
				ang2 = num_input();

				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;
			case 'h':
				std::cout << "enter name, color, and side" << std::endl;
				std::cin >> nam >> col;
				side = num_input();
				hex.setSide(side);
				hex.setName(nam);
				hex.setColor(col);
				ptrHex->draw();
				break;
			case 'e':
				std::cout << "enter name, color, and side" << std::endl;
				std::cin >> nam >> col;
				side = num_input();
				pen.setSide(side);
				pen.setName(nam);
				pen.setColor(col);
				ptrPen->draw();
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			//will get a string from a user
			std::string keep_going = "";
			std::cin >> keep_going;
			if (keep_going.length() == 1 && keep_going[0] == 'x')
			{
				x = 'x';
			}
		}
		catch (const std::exception &e)
		{			
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}


	system("pause");
	return 0;
}