#include "Hexagon.h"

Hexagon::Hexagon(std::string name, std::string col, double side) : Shape(name, col)
{
	setSide(side);
}

void Hexagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl
		<< "side is " << this->_side << std::endl << "Area is " << MathUtils::CalHexagonArea(this->_side) << std::endl;
}

void Hexagon::setSide(int side)
{
	if (side < 0)
	{
		throw shapeException();
	}
	if (std::cin.fail())
	{
		throw InputException();
	}
	this->_side = side;
}
