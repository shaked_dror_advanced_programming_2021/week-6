#include"MathUtils.h"
#include <cmath>


double MathUtils::CalPentagonArea(double side)
{
    //area of Pentagon formula
    return (sqrt(5 * (5 + 2 * (sqrt(5)))) * side * side) / 4;
}

double MathUtils::CalHexagonArea(double side)
{
    //area of Hexagon formula   
    return (3 * sqrt(3) * pow(side, 2)) / 2.0;
}