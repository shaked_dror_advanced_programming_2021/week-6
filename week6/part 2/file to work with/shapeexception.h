#pragma once
#include <exception>

class shapeException : public std::exception
{
public:
	virtual const char* what() const
	{
		return "This is a shape exception!";
	}
};