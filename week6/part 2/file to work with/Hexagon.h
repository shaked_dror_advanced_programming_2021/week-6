#pragma once

#include <iostream>
#include "shape.h"
#include "MathUtils.h"
#include "shapeException.h"
#include "InputException.h"


class Hexagon :public Shape
{
public:
	double _side;

	Hexagon(std::string name, std::string col, double side);
	void draw();
	void setSide(int side);
};
