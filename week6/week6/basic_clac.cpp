#include <iostream>

#define GOOD_VAL true
#define BAD_VAL false
#define NOT_GOOD_INPUT 8200

int add(int a, int b, bool &value_is_good) 
{
    if((a + b) == NOT_GOOD_INPUT || a == NOT_GOOD_INPUT || b == NOT_GOOD_INPUT)
    {
        value_is_good = BAD_VAL;
        return -1;
    }
    return a + b;
}

int multiply(int a, int b, bool &value_is_good)
{
    int sum = 0;
    for (int i = 0; i < b; i++) 
    {
        sum = add(sum, a, value_is_good);
        if (sum == -1 || a == NOT_GOOD_INPUT || b == NOT_GOOD_INPUT)
        {
            value_is_good = BAD_VAL;
            return -1;
        }
    }
    return sum;
}

int pow(int a, int b, bool &value_is_good) 
{
    int exponent = 1;
    for (int i = 0; i < b; i++)
    {
        exponent = multiply(exponent, a, value_is_good);
        if (exponent == -1 || a == NOT_GOOD_INPUT || b == NOT_GOOD_INPUT)
        {
            value_is_good = BAD_VAL;
            return -1;
        }
    }
    return exponent;
}



int main(void) 
{
    bool val = GOOD_VAL;
    std::cout << "pow is: "<<pow(NOT_GOOD_INPUT, 5, val) << std::endl;
    if (val == BAD_VAL)
    {
        std::cout << "This user is not authorized to access 8200,\nplease enter different numbers, or try to get clearance in 1 year.\n" << std::endl;
        val = GOOD_VAL;
    }

    std::cout << "multiply is: " << multiply(NOT_GOOD_INPUT, 5, val) << std::endl;
    if (val == BAD_VAL)
    {
        std::cout << "This user is not authorized to access 8200,\nplease enter different numbers, or try to get clearance in 1 year.\n" << std::endl;
        val = GOOD_VAL;
    }

    std::cout << "add is: " << add(NOT_GOOD_INPUT, 5, val) << std::endl;
    if (val == BAD_VAL)
    {
        std::cout << "This user is not authorized to access 8200,\nplease enter different numbers, or try to get clearance in 1 year.\n" << std::endl;
        val = GOOD_VAL;
    }
}