#include <iostream>
#include <stdexcept>

#define NOT_GOOD_INPUT 8200

int add(int a, int b) 
{
    if (a == NOT_GOOD_INPUT || b == NOT_GOOD_INPUT || a + b == NOT_GOOD_INPUT)
    {
        throw std::string ("This user is not authorized to access 8200,\nplease enter different numbers, or try to get clearance in 1 year.\n"); 
    }
    else
    {
        return a + b;
    }   
}

int  multiply(int a, int b)
{
    int sum = 0;
    for (int i = 0; i < b; i++) 
    {
        sum = add(sum, a);

    }
    return sum;
}

int  pow(int a, int b) 
{
    int exponent = 1;
    for (int i = 0; i < b; i++)
    {
        exponent = multiply(exponent, a);
    }
    return exponent;
}

int main(void) {
    try
    {
        std::cout << "pow is: " << std::endl;
        std::cout << "pow is: " << pow(2, 10) << std::endl << std::endl;

        std::cout << "add is: " << std::endl;
        std::cout << "add is: " << add(2, 10) << std::endl << std::endl;

        std::cout << "multiply is: "  << std::endl;
        std::cout << "multiply is: " << multiply(NOT_GOOD_INPUT, 10) << std::endl << std::endl;
    }
    catch (const std::string& errorString)
    {
        std::cerr<< "ERROR: " << errorString;
    }
    
}